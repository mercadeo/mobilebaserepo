class ApiMailer < ActionMailer::Base
  default from: "gerald@example.com"
  def welcome_email(user)
    @user = user
    @url  = 'http://example.com/login'
    mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  end
  def reset_password_instructions(user, token, opts={})
  	@user = user
  	@token = token
    mail(to: @user.email, subject: 'Account change password information')
  end
end
