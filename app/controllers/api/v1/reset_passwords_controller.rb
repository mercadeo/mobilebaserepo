require 'securerandom'
class Api::V1::ResetPasswordsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def index
    user = User.find_by_email(user_params[:email])
    if user.present?
      begin
        user.send_api_reset_password_instructions
        render json: { :message=> "Recovery mail sent"},
             status: 201
        rescue => error
          render json: { :message=> error.inspect},
            status: 404
      end   
    else
      render json: { :message=> "Email not found"},
             status: 404
    end
  end

  private
  def user_params
    params.require(:user).permit(:email)
  end
end