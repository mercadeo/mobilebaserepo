class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #before_action :authenticate_user!
  before_action :set_current_user

  protected
  def set_current_user
  	if user_signed_in?
  		@current_user = current_user
  	end
  end
end
