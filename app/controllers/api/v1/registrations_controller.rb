class Api::V1::RegistrationsController  < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json

  def create
    #.new(user_params)
    user = User.new(user_params)
    if user.save
      user.ensure_authentication_token!
      #render :json => user.as_json(:token=>user.authentication_token, :email=>user.email), :status=>201
      render :status=>201,
             :json => {:token=>user.authentication_token, :email=>user.email}
      return
    else
      warden.custom_failure!
      render :json => user.errors, :status=>422
    end
  end
  def user_params
    params.require(:user).permit(:email, :password)
  end

end