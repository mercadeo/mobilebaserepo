require 'token_authenticatable'
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include TokenAuthenticatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable
  
  def send_api_reset_password_instructions
    token = set_api_reset_password_token
    send_api_reset_password_instructions_notification(token)
    token
  end
  protected
  def set_api_reset_password_token
    raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)
    randomstring = SecureRandom.hex(6)
    self.password = randomstring
    self.save
    self.reset_password_token   = enc
    self.reset_password_sent_at = Time.now.utc
    self.save(validate: false)
    #raw
    randomstring
  end
  def send_api_reset_password_instructions_notification(token)
    send_api_devise_notification(:reset_password_instructions, token, {})
  end
  def clear_api_reset_password_token
    self.reset_password_token = nil
    self.reset_password_sent_at = nil
  end
  def send_api_devise_notification(notification, *args)
    if new_record? || changed?
      pending_notifications << [notification, args]
    else
      puts "-----------------------------------------------------------"
      ApiMailer.send(notification, self, *args).deliver
    end
  end
end
