class Api::V1::ChangePasswordController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def update
    user = User.find_by_authentication_token(params[:id])
    if user.present? && params[:password].present?
      begin
        user.password=params[:password]
        user.password_confirmation=params[:password_confirmation]
        user.save!
        render json: { :message=> "Password changed successfully"},
          status: 201
        rescue => error
          render json: { :message=> error.inspect},
            status: 404
      end   
    else
      render json: { :message=> "Invalid Values or Token"},
             status: 404
    end
  end
end